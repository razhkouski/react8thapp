import { addUserToDatabase, getUsersFromDatabase, updateUserInDatabase, deleteUserFromDatabase } from "../services/user.service";
import { addUser, getUsers, updateUser, deleteUser  } from "../actionCreators/profile.action";

export const createUser = (data) => (dispatch) => { addUserToDatabase(data).then((data) => { dispatch(addUser(data)) }) };
export const getUsersList = () => (dispatch) => { getUsersFromDatabase().then((data) => { dispatch(getUsers(data)) }) };
export const renewUser = (data) => (dispatch) => { updateUserInDatabase(data).then((data) => { dispatch(updateUser(data)) }) };
export const removeUser = (data) => (dispatch) => { deleteUserFromDatabase(data).then((data) => { dispatch(deleteUser(data)) }) };