export async function addUserToDatabase(data: any) {
    return await fetch(`http://localhost:3000/user/create`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((res) => res.json());
};

export async function getUsersFromDatabase(){
    return await fetch(`http://localhost:3000/user/getAll`, {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((res) => res.json());
};

export async function updateUserInDatabase(data: any) {
    return await fetch(`http://localhost:3000/user/update`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((res) => res.json());
};

export async function deleteUserFromDatabase(data: any) {
    return await fetch(`http://localhost:3000/user/delete`, {
        method: 'DELETE',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((res) => res.json());
};