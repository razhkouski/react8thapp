import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import { store } from './store/store';
import { Provider } from 'react-redux';
import Create from './components/Create';
import Read from './components/Read';
import Update from './components/Update'
import Delete from './components/Delete';


function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Route path='/post' component={Create}/>
                <Route path='/get' component={Read}/>
                <Route path='/update' component={Update}/>
                <Route path='/delete' component={Delete}/>
            </BrowserRouter>
        </Provider>
    )
}

export default App;