import { ADD_USER, GET_USERS, DELETE_USER, UPDATE_USER } from "../constants/profile.constants";

const defaultState = {};

export default (state = defaultState, action: any) => {
    switch (action.type) {
        case ADD_USER:
            let addUserState = {
                createMsg: action.data,
            };
        return addUserState;
        case GET_USERS:
            let getUsersState = {
                array: action.data.userList
            };
        return getUsersState;
        case DELETE_USER:
            let deleteUserState = {
                deleteMsg: action.data,
            };
        return deleteUserState;
        case UPDATE_USER:
            let updateUserState = {
                updateMsg: action.data,
            };
        return updateUserState;
        default:
            return state;
    }
};