import { ADD_USER, GET_USERS, UPDATE_USER, DELETE_USER } from "../constants/profile.constants";

export const addUser = (data: any) => ({ type: ADD_USER, data });
export const getUsers = (data: any) => ({ type: GET_USERS, data });
export const updateUser = (data: any) => ({ type: UPDATE_USER, data });
export const deleteUser = (data: any) => ({ type: DELETE_USER, data });