import React from 'react';
import { NavLink } from "react-router-dom";

function Navbar() {
    return (
        <nav>
            <div>
                <NavLink to="/post">Create</NavLink>
            </div>
            <div>
                <NavLink to="/get">Read</NavLink>
            </div>
            <div>
                <NavLink to="/update">Update</NavLink>
            </div>
            <div>
                <NavLink to="/delete">Delete</NavLink>
            </div>
        </nav>
    )
}

export default Navbar;
