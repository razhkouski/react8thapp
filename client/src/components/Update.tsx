import React, { useState } from 'react';
import { connect } from "react-redux";
import { renewUser } from "../thunk/user.thunk";

function UpdateUser(props: any) {
    const [name, setName] = useState("Volodya");
    const [email, setEmail] = useState("ivan.ivanov@gmail.com");

    const newName = () => {
        let user = {
            name: name,
            email: email
        };

        props.updateUser(user)

        setName('');
        setEmail('');
    }

    return (
        <>
            <h2>Update user</h2>
            <span>Email: </span><input value={email} onChange={(e) => setEmail(e.target.value)} />
            <br/>
            <span>New user name: </span><input value={name} onChange={(e) => setName(e.target.value)} />
            <button onClick={newName}>Update</button>
            <p>{props.user.updateMsg}</p>
        </>
    )
}

export default connect(
    (state: any) => ({
        user: state.user
    }),
    {renewUser}
)(UpdateUser);
