import React, { useState } from 'react';
import { connect } from "react-redux";
import { getUsersList } from "../thunk/user.thunk";

function GetUsers(props: any) {
    const [flag, setFlag] = useState(false);

    const getUserList = () => {
        props.getUsersList();
        setFlag(true);
    }

    return (
        <>
            <button onClick={getUserList}>Get Users</button>
            {flag && props.user.array ? (
                props.user.array.map((array: any) => (
                    <ul key={array._id}>
                        <li>
                            User: {array.name}
                            <br/>
                            Email: {array.email}
                        </li>
                    </ul>
                ))
            ) : null}
        </>
    )
}

export default connect(
    (state: any) => ({
        user: state.user
    }),
    {getUsersList}
)(GetUsers);