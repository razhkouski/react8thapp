import React, { useState } from 'react';
import { connect } from "react-redux";
import { removeUser } from "../thunk/user.thunk"

function GetUser(props: any) {
    const [email, setEmail] = useState('');

    const deleteUser = () => {
        let user = {
            email: email
        };

        props.deleteUser(user);

    }
    return (
        <>
            <h2>Delete user</h2>
            <span>Enter user email : </span><input value={email} onChange={(e: any) => setEmail(e.target.value)} />
            <button onClick={deleteUser}>Delete</button>
            <p>{props.user.deleteMsg}</p>
        </>
    )
};

export default connect(
    (state: any) => ({
        user: state.user
    }),
    {removeUser}
)(GetUser);