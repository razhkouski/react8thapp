import React, { useState } from 'react';
import { connect } from "react-redux";
import { createUser } from "../thunk/user.thunk";

interface IUser {
    createMsg: string
}

interface IProps {
    user: IUser,
    createUser: Function
}

function CreateUser({user, createUser}: IProps) {
    const [name, setName] = useState("Ivan");
    const [email, setEmail] = useState("ivan.ivanov@gmail.com");

    const createNewUser = () => {
        let user = {
            name: name,
            email: email
        };

        createUser(user);

        setName('');
        setEmail('');
    }

    return (
        <>
            <h2>Create user</h2>
            <span>Name: </span><input value={name} onChange={(e) => setName(e.target.value)} />
            <br />
            <span>Email: </span><input value={email} onChange={(e) => setEmail(e.target.value)} />
            <button onClick={createNewUser}>Create</button>
            <p>{user.createMsg}</p>
        </>
    )
}

export default connect(
    (state: any) => ({
        user: state.user
    }),
    {createUser}
)(CreateUser);
