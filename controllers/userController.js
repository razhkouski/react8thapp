import { User } from "../models/userModel.js";

export const createUser = async function (req, res) {
    try {
        await User.create({ name: req.body.name, email: req.body.email});
        return res.json(`Пользователь c почтой ${req.body.email} создан.`);
    } catch (e) {
        return res.json(`Пользователь c почтой ${req.body.email} уже существует.`);
    }
};

export const getAllUsers = async function (req, res) {
    try {
        let userList = await User.find({});
        return res.json({userList});
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const deleteUser = async function (req, res) {
    try {
        await User.deleteOne({ email: req.body.email });
        return res.json(`Пользователь ${req.body.email} удален`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};

export const updateUser = async function (req, res) {
    try {
        await User.updateOne({email: req.body.email}, {name: req.body.name });
        return res.json(`Имя пользователя c почтой ${req.body.email} обновлено`);
    } catch (e) {
        return res.status(500).send('Неверные параметры запроса');
    }
};